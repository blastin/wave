package knin.project.wave;

import java.util.Collection;

public interface Driver {

    static DriverBuilder builder() {
        return new DriverBuilderImpl();
    }

    Collection<String> channelsOutput();

    Collection<Receiver<?>> receivers();

}
