package knin.project.wave;

public interface DriverBuilder {

    /**
     * @param receiver instance of the receiver
     * @param <T>      Type
     * @return Driver Builder themself
     */
    <T> DriverBuilder addReceiver
    (
            final Receiver<T> receiver
    );

    DriverBuilder addChannel(final String outputChannel);

    Driver build();

}
