package knin.project.wave;

import java.util.Objects;

public interface Pipe<T, S> {

    static <T, S> Pipe<T, S> proxy(final Pipe<? super T, S> pipe) {
        Objects.requireNonNull(pipe);
        return information -> pipe.flux(Objects.requireNonNull(information));
    }

    S flux(final T information);

}
