package knin.project.wave;

import java.util.*;

final class DriverBuilderImpl implements DriverBuilder, Driver {

    DriverBuilderImpl() {
        this.channelOutputs = new TreeSet<>();
        this.receivers = new ArrayList<>();
    }

    private final Set<String> channelOutputs;

    private final Collection<Receiver<?>> receivers;

    @Override
    public <T> DriverBuilder addReceiver
            (
                    final Receiver<T> receiver
            ) {

        receivers.add
                (
                        Receiver.proxy(receiver)
                );

        return this;

    }

    @Override
    public DriverBuilder addChannel(final String outputChannel) {
        channelOutputs.add(Objects.requireNonNull(outputChannel));
        return this;
    }

    @Override
    public Driver build() {
        return this;
    }

    @Override
    public Collection<String> channelsOutput() {
        return new HashSet<>(channelOutputs);
    }

    @Override
    public Collection<Receiver<?>> receivers() {
        return new ArrayList<>(receivers);
    }

}
