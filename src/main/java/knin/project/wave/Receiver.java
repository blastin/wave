package knin.project.wave;

import java.util.Objects;

public interface Receiver<T> {

    static <T> Receiver<T> proxy(final Receiver<T> receiver) {

        Objects.requireNonNull(receiver);
        return new Receiver<>() {
            @Override
            public void consumer(T message) throws UnrecognizedException {
                receiver.consumer(Objects.requireNonNull(message));
            }

            @Override
            public Class<T> type() {
                return receiver.type();
            }

            @Override
            public String channel() {
                return receiver.channel();
            }

        };

    }

    void consumer(final T message) throws UnrecognizedException;

    Class<T> type();

    String channel();

}
