package knin.project.wave;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.util.Optional;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

class DriverBuilderImplTest {

    @ParameterizedTest
    @CsvSource({"1,2,3"})
    void shouldGetOutputChannels(final String a, final String b, final String c) {

        final Driver driver =
                Driver.builder()
                        .addChannel(a)
                        .addChannel(b)
                        .addChannel(c)
                        .build();

        Assertions.assertEquals(Set.of(a, b, c), driver.channelsOutput());

    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    @Test
    void should() throws UnrecognizedException {

        AtomicInteger atomic = new AtomicInteger();

        final Driver driver =
                Driver.builder()
                        .addReceiver
                                (
                                        new ReceiverSkeleton<StringBytes>() {
                                            @Override
                                            public void consumer(final StringBytes message) {
                                                atomic.set(message.toInteger());
                                            }

                                            @Override
                                            public Class<StringBytes> type() {
                                                return StringBytes.class;
                                            }

                                            @Override
                                            public String channel() {
                                                return "channel";
                                            }
                                        }
                                )
                        .build();

        Assertions.assertEquals(1, driver.receivers().size());

        Optional<Receiver<?>> first = driver.receivers().stream().findFirst();

        Assertions.assertTrue(first.isPresent());

        Receiver receiver = first.get();

        Assertions.assertEquals("channel", receiver.channel());

        Assertions.assertEquals(StringBytes.class, receiver.type());

        Assertions.assertNotNull(receiver);

        receiver.consumer(new StringBytes("1"));

        Assertions.assertEquals(1, atomic.get());

    }

}