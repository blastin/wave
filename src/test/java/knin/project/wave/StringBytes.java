package knin.project.wave;

import java.util.Objects;

final class StringBytes {

    StringBytes(final String value) {
        this.value = Objects.requireNonNull(value);
    }

    StringBytes(final Integer value) {
        this(String.valueOf(value));
    }

    private final String value;

    public Integer toInteger() {
        return Integer.parseInt(value);
    }

}
