package knin.project.wave;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.util.concurrent.atomic.AtomicReference;

class ReceiverTest {

    @Test
    void shouldRaiseException() {

        final Receiver<StringBytes> receiver = new ReceiverSkeleton<>() {

            @Override
            public void consumer(StringBytes message) throws UnrecognizedException {
                throw new UnrecognizedException();
            }

        };

        Assertions.assertThrows
                (
                        UnrecognizedException.class,
                        () -> Receiver.proxy(receiver).consumer(new StringBytes(1))
                );

    }

    @ParameterizedTest
    @CsvSource({"1,1", "2,2"})
    void shouldConsumeMessage(final String input, final Integer output) throws UnrecognizedException {

        final AtomicReference<Integer> atomicInteger = new AtomicReference<>();

        final Receiver<StringBytes> receiver = new ReceiverSkeleton<>() {
            @Override
            public void consumer(StringBytes message) {
                atomicInteger.set(message.toInteger());
            }
        };

        Receiver.proxy(receiver).consumer(new StringBytes(input));

        Assertions.assertEquals(output, atomicInteger.get());

    }

    @Test
    void whenReceiverIsNullShouldRaiseException() {
        Assertions.assertThrows
                (
                        NullPointerException.class, () ->
                                Receiver.proxy(null)
                );
    }

    @Test
    void whenMessageIsNullShouldRaiseException() {

        final Receiver<StringBytes> receiver = new ReceiverSkeleton<>();

        Assertions.assertThrows(NullPointerException.class, () -> Receiver.proxy(receiver).consumer(null));

    }

    @Test
    void shouldReturnType() {

        Assertions.assertEquals
                (
                        StringBytes.class,
                        Receiver.proxy
                                        (
                                                new ReceiverSkeleton<StringBytes>() {

                                                    @Override
                                                    public Class<StringBytes> type() {
                                                        return StringBytes.class;
                                                    }

                                                }
                                        )
                                .type()
                );
    }

    @Test
    void shouldReturnChannel() {
        Assertions.assertEquals
                (
                        "channel",
                        Receiver.proxy
                                        (
                                                new ReceiverSkeleton<StringBytes>() {

                                                    @Override
                                                    public String channel() {
                                                        return "channel";
                                                    }
                                                }
                                        )
                                .channel()
                );
    }
}