package knin.project.wave;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class ReceiverSkeletonTest {

    @Test
    void whenCallConsumeShouldRaiseException() {
        Assertions.assertThrows(IllegalCallerException.class, () -> new ReceiverSkeleton<>().consumer(null));
    }

    @Test
    void whenCallTypeShouldRaiseException() {
        Assertions.assertThrows(IllegalCallerException.class, () -> new ReceiverSkeleton<>().type());
    }

    @Test
    void whenCallChannelShouldRaiseException() {
        Assertions.assertThrows(IllegalCallerException.class, () -> new ReceiverSkeleton<>().channel());
    }

}