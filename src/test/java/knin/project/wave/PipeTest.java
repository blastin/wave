package knin.project.wave;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class PipeTest {

    @Test
    void shouldConsumeMessage() {

        final Pipe<StringBytes, Integer> pipe = StringBytes::toInteger;

        final Integer flux = Pipe.proxy(pipe)
                .flux(new StringBytes("16"));

        Assertions.assertEquals(16, flux);

    }

    @Test
    void whenPipeIsNullShouldRaiseException() {
        Assertions.assertThrows
                (
                        NullPointerException.class,
                        () -> Pipe.proxy(null)
                );
    }

    @Test
    void whenInformationIsNullShouldRaiseException() {

        final Pipe<StringBytes, Integer> pipe = StringBytes::toInteger;

        Assertions.assertThrows(NullPointerException.class, () -> Pipe.proxy(pipe).flux(null));

    }
}