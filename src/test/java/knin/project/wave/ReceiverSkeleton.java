package knin.project.wave;

class ReceiverSkeleton<T> implements Receiver<T> {

    @Override
    public void consumer(T message) throws UnrecognizedException {
        throw new IllegalCallerException();
    }

    @Override
    public Class<T> type() {
        throw new IllegalCallerException();
    }

    @Override
    public String channel() {
        throw new IllegalCallerException();
    }

}
